//
//  ViewController.swift
//  IndbexMockup
//
//  Created by Mac-27 on 22/9/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var caducProgressBar: UIView!
    @IBOutlet weak var valorCaduc: UILabel!
    @IBOutlet weak var datosProgressBar: UIView!
    @IBOutlet weak var valorDatos: UILabel!
    @IBOutlet weak var mercadoProgressBar: UIView!
    @IBOutlet weak var valorMercado: UILabel!
    @IBOutlet weak var dataBrowserView: UIView!
    
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var generalLabel: UILabel!
    
    var generalActive :Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalActive = false
        
        // Do any additional setup after loading the view, typically from a nib.

    }
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func generalAction(_ sender: Any) {
        generalActive = !generalActive
        
        if generalActive{
           generalView.backgroundColor = UIColor.white
           generalLabel.textColor = UIColor.black
           dataBrowserView.alpha = 1
        }else{
            generalView.backgroundColor = UIColor.clear
            generalLabel.textColor = UIColor.white
            dataBrowserView.alpha = 0
            
        }
        
        
        
        
    }
    @IBAction func action(_ sender: Any) {
        
        
        
       // drawCircle(arcCenter: CGPoint(x: 400,y:200), radius: CGFloat(80), startAngle: CGFloat(-Double.pi / 2), endAngle: CGFloat(Double.pi ))
       
//        for view in self.view.subviews {
//            view.removeFromSuperview()
//        }
        
        
        valorCaduc.text = "50%"
        drawCircleInView(view: caducProgressBar, progress: 50)
        
        valorDatos.text = "75%"
        drawCircleInView(view: datosProgressBar, progress:75)
        
        valorMercado.text = "15%"
        drawCircleInView(view: mercadoProgressBar, progress:15)
        
//        let ring:RingView = RingView()
//        var boxPoint = CGPoint(x: 200, y: 100)
//        var boxSize = CGSize(width: 365, height: 124)
//        var boxRect = CGRect(origin: boxPoint, size: boxSize)
//
//
//
//        ring.draw(boxRect)
        
    }
    
    
    private func drawCircleInView(view: UIView, progress: Double){
        
        drawCircle(arcCenter: view.center, radius: view.frame.size.height/2, startAngle:  CGFloat(0), progress: progress)
        
        
        
    }
    
    
    
    private func drawCircle(arcCenter: CGPoint, radius: CGFloat, startAngle: CGFloat, progress:Double) {
        
        
        //base
        let circlePathResto = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: CGFloat(0), endAngle:  CGFloat(Double.pi * 2 ), clockwise: true)
        
        
        let shapeLayerResto = CAShapeLayer()
        shapeLayerResto.path = circlePathResto.cgPath
        
        //change the fill color
        shapeLayerResto.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayerResto.strokeColor = UIColor.darkGray.cgColor
        
        
        //you can change the line width
        shapeLayerResto.lineWidth = 12.0
        
         view.layer.addSublayer(shapeLayerResto)
        
        //return
        
        
        let endAngle : CGFloat = CGFloat(Double.pi * 2 * progress / 100)
      
        
        let circlePath = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.green.cgColor
        if (progress < 70){ shapeLayer.strokeColor = UIColor.yellow.cgColor}
        if (progress < 51){ shapeLayer.strokeColor = UIColor.orange.cgColor}
        if (progress < 25){ shapeLayer.strokeColor = UIColor.red.cgColor}



        //you can change the line width
        shapeLayer.lineWidth = 12.0
        
        view.layer.addSublayer(shapeLayer)
        
        
        
        
        
    }
    
    
    

}

