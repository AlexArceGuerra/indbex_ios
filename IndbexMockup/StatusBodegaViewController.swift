//
//  StatusBodegaViewController.swift
//  IndbexMockup
//
//  Created by Mac-27 on 5/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class StatusBodegaViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var bodegasStatusCW: UICollectionView!
    
    let reuseIdentifier = "Cell" // also enter this string as the cell identifier in the storyboard
    var items = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        bodegasStatusCW.delegate = self
        bodegasStatusCW.dataSource = self
    
    }
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  3 // self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! StatusBodegaCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.myLabel.text = self.items[indexPath.item]
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    
    
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // This is Just for example , for the scenario Step-I -> 1
        let yourWidthOfLable = (bodegasStatusCW.frame.size.width/3)-15
        //let font = UIFont(name: "Helvetica", size: 20.0)
        
        //var expectedHeight = heightForLable(array[indePath.row], font: font, width:yourWidthOfLable )
        let expectedHeight = bodegasStatusCW.frame.size.height
        
        return CGSize(width: yourWidthOfLable, height: expectedHeight)
    }
    
     
    
    
    
    
}
